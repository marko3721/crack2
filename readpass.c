#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    // Allocate array of 50 char pointers (strings)
    int size = 50;
    char **pwds = (char **)malloc(size * sizeof(char *));
    
    char str[40];
    int i = 0;
    while(scanf("%s", str) != EOF)
    {
        // If array is full, make it bigger
        if (i == size)
        {
            size = size + 10;
            char **newarr = (char **)realloc(pwds, size * sizeof(char *));
            if (newarr != NULL) pwds = newarr;
            else
            {
                printf("Realloc failed\n");
                exit(1);
            }
        }
        char *newstr = (char *)malloc((strlen(str)+1) * sizeof(char));
        strcpy(newstr, str);
        pwds[i] = newstr;
        i++;
    }
    
    for (int j = 0; j < size; j++)
    {
        printf("%d %s\n", j, pwds[j]);
    }
}
