#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    
    // Hash the guess using MD5
    char *test1 = md5(guess, strlen(guess));
    
    if (strcmp(test1,hash) == 0) 
    {
        free(test1);   
        return 1;
    }
    else
    // Free any malloc'd memory*********************************
    free(test1);
    return 0;
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    FILE *fp = fopen(filename,"r"); // read mode
    
    //Allocate array of 50 char pointers (strings)
    int size = 50;
    char **pwds = (char **)malloc(size * sizeof(char *));
    char str[40];
    int i = 0;
 
    if( fp == NULL )
    {
       printf("Error while opening the file.\n");
       exit(1);
    }
 
    while(fscanf(fp, "%s\n", str) != EOF)
    {  
       // If array is full, make it bigger
        if (i == size)
        {
            size = size + 50;
            char **newarr = (char **)realloc(pwds, size * sizeof(char *));
            if (newarr != NULL) pwds = newarr;
            else
            {
                printf("Realloc failed\n");
                exit(1);
            }
        }
        char *newstr = (char *)malloc((strlen(str)+1) * sizeof(char));
        strcpy(newstr, str);
        pwds[i] = newstr;
        i++;
    }
  //  printf("%d\n", i);
   i++;
   pwds[i] = '\0';
   fclose(fp);
   return pwds;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    
    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]); 
    
    //Count the number of total passwords that need to be checked
    int countDict = 0;
    while(dict[countDict] != '\0')
    {
       countDict++;
    }
 //   printf("CountDict = %d\n", countDict);
    
    //Count Number of Hashes
    int countHashes = 0;
    while(hashes[countHashes] != '\0')
    {
       countHashes++;
    }
  //  printf("CountHashes = %d\n", countHashes);
    
    //Set double loop to check passwords and hashes
    for(int i = 0; i < countHashes; i++)
    {
        for(int j = 0; j < countDict; j++)
        {
            if(tryguess(hashes[i], dict[j]))
            {
               printf("%s = %s\n", hashes[i], dict[j]);
            }
        }
    }
}
